package org.boaboa.srocha.controller;

import org.boaboa.srocha.DTO.FarewellDTO;
import org.boaboa.srocha.service.FarewellService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class FarewellController {

	@Autowired
	private FarewellService _farewellService;

	@RequestMapping(method = POST)
	public ResponseEntity<FarewellDTO> farewell(@RequestParam(value = "name", required = true) String name)
			throws MissingServletRequestParameterException {
		FarewellDTO farewellDTO = new FarewellDTO();
		String fareWell = _farewellService.getFarewell(name);
		farewellDTO.setMessage(fareWell);
		return new ResponseEntity<FarewellDTO>(farewellDTO, OK);

	}

}
