package org.boaboa.srocha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FarewellMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FarewellMicroserviceApplication.class, args);
	}
}
