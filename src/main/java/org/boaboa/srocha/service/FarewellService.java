package org.boaboa.srocha.service;

import org.springframework.web.bind.MissingServletRequestParameterException;

public interface FarewellService {
	
	String getFarewell(String name) throws MissingServletRequestParameterException;

}
