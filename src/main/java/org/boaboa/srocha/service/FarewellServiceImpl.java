package org.boaboa.srocha.service;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.MissingServletRequestParameterException;

@Service
public class FarewellServiceImpl implements FarewellService {

	public static final String FAREWELLFORMAT = "Farewell, %s";

	@Override
	public String getFarewell(String name) throws MissingServletRequestParameterException {
		if (name.isEmpty()) {
			throw new MissingServletRequestParameterException("name", "String");
		}
		return String.format(FAREWELLFORMAT, name);
	}

}
