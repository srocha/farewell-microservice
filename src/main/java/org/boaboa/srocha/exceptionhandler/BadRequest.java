package org.boaboa.srocha.exceptionhandler;

import org.boaboa.srocha.DTO.ErrorFarewellDTO;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class BadRequest {

	@ExceptionHandler(MissingServletRequestParameterException.class)
	public ResponseEntity<ErrorFarewellDTO> handleException(MissingServletRequestParameterException e) {
		ErrorFarewellDTO errorFarewellDTO = new ErrorFarewellDTO();
		errorFarewellDTO.setField(e.getParameterName());
		errorFarewellDTO.setMessage(e.getMessage());
		return new ResponseEntity<ErrorFarewellDTO>(errorFarewellDTO, BAD_REQUEST);
	}

}
